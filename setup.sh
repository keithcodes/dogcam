python3 -m venv venv
source ./venv/bin/activate

pip install -U pip
pip install -U setuptools

pip install tensorflow

pip install -r requirements.txt
