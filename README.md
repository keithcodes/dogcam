
```
sudo apt-get install python3-venv python3-dev -y
./setup.sh
./tensorflow.sh
```

# Steps 

## Capture

The program uses gphoto2 to take a photo via an attached camera.

## Object Detection

The program runs the previously taken photo through the coco object recognition neural net to determine if there is a worthy doggo in the scene.


# References
 * https://github.com/tinrab/go-tensorflow-image-recognition/blob/master/main.go
 * https://colab.research.google.com/drive/1ijYmweG5WNHBKWwHvbAbA2ITjeklPrBq#scrollTo=Osh6i-uBYSSV
 * https://lambdalabs.com/blog/tensorflow-2-0-tutorial-03-saving-checkpoints/#:~:text=Checkpoints%20are%20saved%20model%20states,as%20its%20high%2Dlevel%20API.
   * https://github.com/lambdal/TensorFlow2-tutorial/blob/master/03-checkpoint/resnet_cifar_withcheckpoint.py

freeze_graph.py from https://github.com/tensorflow/tensorflow/blob/master/tensorflow/python/tools/freeze_graph.py