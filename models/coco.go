package models


import (
	"fmt"
	"io/ioutil"
	"strings"

	"github.com/juandes/tensorflow-go-models/responses"
	tf "github.com/tensorflow/tensorflow/tensorflow/go"
	"github.com/tensorflow/tensorflow/tensorflow/go/op"
)

// Coco is a MobileNet V1 model trained on the COCO dataset.
type Coco struct {
	model  *tf.SavedModel
	labels []string
}

const path = "models/ssd_mobilenet_v1_coco_2018_01_28/saved_model/"

// NewCoco returns a Coco object
func NewCoco() *Coco {
	return &Coco{}
}

// Load loads the ssd_mobilenet_v1_coco_2018_01_28 SavedModel.
func (c *Coco) Load() error {
	model, err := tf.LoadSavedModel(path, []string{"serve"}, nil)
	if err != nil {
		return fmt.Errorf("Error loading model: %v", err)
	}
	c.model = model
	c.labels, err = readLabels(strings.Join([]string{path, "labels.txt"}, ""))
	if err != nil {
		return fmt.Errorf("Error loading labels file: %v", err)
	}
	return nil
}

// Predict predicts.
func (c *Coco) Predict(data []byte) *responses.ObjectDetectionResponse {
	tensor, _ := makeTensorFromBytes(data)

	output, err := c.model.Session.Run(
		map[tf.Output]*tf.Tensor{
			c.model.Graph.Operation("image_tensor").Output(0): tensor,
		},
		[]tf.Output{
			c.model.Graph.Operation("detection_boxes").Output(0),
			c.model.Graph.Operation("detection_classes").Output(0),
			c.model.Graph.Operation("detection_scores").Output(0),
			c.model.Graph.Operation("num_detections").Output(0),
		},
		nil,
	)

	if err != nil {
		fmt.Printf("Error running the session: %v", err)
		return nil
	}

	outcome := responses.NewObjectDetectionResponse(output, c.labels)
	return outcome
}

// CloseSession closes a session.
func (c *Coco) CloseSession() {
	c.model.Session.Close()
}

func readLabels(labelsFile string) ([]string, error) {
	fileBytes, err := ioutil.ReadFile(labelsFile)
	if err != nil {
		return nil, fmt.Errorf("Unable to read labels file: %v", err)
	}

	return strings.Split(string(fileBytes), "\n"), nil
}


// Convert the image in filename to a Tensor suitable as input
func makeTensorFromBytes(bytes []byte) (*tf.Tensor, error) {
	// bytes to tensor
	tensor, err := tf.NewTensor(string(bytes))
	if err != nil {
		return nil, err
	}

	// create batch
	graph, input, output, err := makeBatch()
	if err != nil {
		return nil, err
	}

	// Execute that graph create the batch of that image
	session, err := tf.NewSession(graph, nil)
	if err != nil {
		return nil, err
	}

	defer session.Close()

	batch, err := session.Run(
		map[tf.Output]*tf.Tensor{input: tensor},
		[]tf.Output{output},
		nil)
	if err != nil {
		return nil, err
	}
	return batch[0], nil
}

// makeBatch uses ExpandDims to convert the tensor into a batch of size 1.
func makeBatch() (graph *tf.Graph, input, output tf.Output, err error) {
	s := op.NewScope()
	input = op.Placeholder(s, tf.String)

	output = op.ExpandDims(s,
		op.DecodeJpeg(s, input, op.DecodeJpegChannels(3)),
		op.Const(s.SubScope("make_batch"), int32(0)))
	graph, err = s.Finalize()
	return graph, input, output, err
}