import tensorflow_datasets as tfds
import tensorflow as tf

from tensorflow.python.tools import freeze_graph

import os

dataset, info = tfds.load(name="stanford_dogs", with_info=True)


# Pre-processing and preperation steps for classification

IMG_LEN = 224
IMG_SHAPE = (IMG_LEN,IMG_LEN,3)
N_BREEDS = 120

training_data = dataset['train']
test_data = dataset['test']


def preprocess(ds_row):
    # Image conversion int->float + resizing
    image = tf.image.convert_image_dtype(ds_row['image'], dtype=tf.float32)
    image = tf.image.resize(image, (IMG_LEN, IMG_LEN), method='nearest')

    # Onehot encoding labels
    label = tf.one_hot(ds_row['label'],N_BREEDS)

    return image, label


def prepare(dataset, batch_size=None):
    ds = dataset.map(preprocess, num_parallel_calls=4)
    ds = ds.shuffle(buffer_size=1000)
    if batch_size:
        ds = ds.batch(batch_size)
    ds = ds.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)
    return ds


# Define the model
print ("Defining model")
base_model = tf.keras.applications.MobileNetV2(input_shape=IMG_SHAPE,
                                               include_top=False,
                                               weights='imagenet')

# Freezes weights during training
base_model.trainable = False

model = tf.keras.Sequential([
    base_model,
    tf.keras.layers.GlobalAveragePooling2D(),
    tf.keras.layers.Dense(N_BREEDS, activation='softmax')
])


# Compile and fit the model
print ("Compiling model")
model.compile(optimizer=tf.keras.optimizers.Adamax(0.0001),
              loss='categorical_crossentropy',
              metrics=['accuracy', 'top_k_categorical_accuracy'])

print ("Preparing training batches")
train_batches = prepare(training_data, batch_size=32)
test_batches = prepare(test_data, batch_size=32)

# Create a callback that saves the model's weights as a checkpoint
print ("Creating checkpoint callback")
checkpoint_path = "models/dogs/checkpoints/model-{epoch:02d}-{val_accuracy:.2f}.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)
if not os.path.exists(checkpoint_dir):
    os.makedirs(checkpoint_dir)
cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path,
                                                 monitor='val_accuracy',
                                                 save_best_only=False, save_weights_only=True,
                                                 verbose=1,
                                                 save_frequency=1)

# Allows us to stop early to prevent over-training
earlystop_callback = tf.keras.callbacks.EarlyStopping(
    monitor='val_accuracy', min_delta=0.0001,
    patience=1)

print ("Fitting model")
history = model.fit(train_batches,
                    epochs=30,
                    validation_data=test_batches,
                    callbacks=[earlystop_callback, cp_callback],
                    verbose=1,
                    initial_epoch=7)

# Save the weights
print ("Saving model weights")
model.save_weights('./models/dogs/checkpoints/my_checkpoint')

# Save the model
print ("Saving model")
model.save('models/dogs')

# Freeze the graph for consumption with other tools
# print ("Freezing graph")
# freeze_graph.freeze_graph(input_graph=pbtxt_filepath, input_saver='', input_binary=False, input_checkpoint=ckpt_filepath, output_node_names='cnn/output', restore_op_name='save/restore_all', filename_tensor_name='save/Const:0', output_graph=pb_filepath, clear_devices=True, initializer_nodes='')