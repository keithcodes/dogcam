module keithcod.es/dogcam

go 1.14

require (
	github.com/juandes/tensorflow-go-models v0.0.0-20200512112846-2a2ded0590e1
	github.com/tensorflow/tensorflow v2.1.1+incompatible
)
