package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"os/exec"

	"keithcod.es/dogcam/models"
)

//// #cgo CFLAGS: -IC:/usr/include
//// #cgo CFLAGS: -LC:/usr/lib
//import "C"

const boundary = "informs"


func main () {
	// Create and load the object detection model
	objectModel := models.NewCoco()
	err := objectModel.Load()
	if err != nil {
		fmt.Printf("Error loading object model: %v", err)
		panic(err)
	}
	defer objectModel.CloseSession()

	// Run the image loop
	for {
		ctx := context.Background()

		// Take a photo from the attached camera
		cmd := exec.CommandContext(ctx, `gphoto2 --capture-image-and-download --filename "frame.jpg"`)
		if err := cmd.Run(); err != nil {
			log.Fatalf("failed to capture image: %w", err)
		}

		// Read in the image frame that was taken
		b, err := ioutil.ReadFile("frame.jpg")
		if err != nil {
			log.Fatalf("failed to read frame: %w", err)
		}

		// Run object prediction on the image
		outcome := objectModel.Predict(b)
		fmt.Printf("%#v\n", outcome)

	}


	/*
	// Load the tf model
	log.Println("Loading tensorflow model")
	model, err := ioutil.ReadFile("./model/dogs/saved_model.pb")
	if err != nil {
		log.Fatal(err)
	}

	// Create the graph
	log.Println("Creating model graph")
	graphModel := tf.NewGraph()
	if err := graphModel.Import(model, ""); err != nil {
		log.Fatal(err)
	}

	// Create the session
	log.Println("Creating session")
	sessionModel, err := tf.NewSession(graphModel, nil)
	if err != nil {
		log.Fatal(err)
	}

	// Enter a loop to grab images and perform prediction
	log.Println("starting image loop")
	for {
		ctx := context.Background()

		cmd := exec.CommandContext(ctx, `gphoto2 --capture-image-and-download --filename "frame.jpg"`)
		if err := cmd.Run(); err != nil {
			log.Fatalf("failed to capture image: %w", err)
		}

		b, err := ioutil.ReadFile("frame.jpg")
		if err != nil {
			log.Fatalf("failed to read frame: %w", err)
		}

		imageBuffer := bytes.NewBuffer(b)

		// Convert the image to a tensor
		tensor, err := makeTensorFromImage(imageBuffer, "frame.jpg")
		if err != nil {
			log.Fatalf("invalid image: %w", err)
		}

		// Run inference
		output, err := sessionModel.Run(
			map[tf.Output]*tf.Tensor{
				graphModel.Operation("input").Output(0): tensor,
			},
			[]tf.Output{
				graphModel.Operation("output").Output(0),
			},
			nil)
		if err != nil {
			log.Fatalf("could not run inference: %w", err)
		}

		fmt.Printf("%#v\n", output)

		//fmt.Printf("%#v\n", findBestLabels(output[0].Value().([][]float32)[0]))
	}*/
}
/*
type ClassifyResult struct {
	Filename string        `json:"filename"`
	Labels   []LabelResult `json:"labels"`
}

type LabelResult struct {
	Label       string  `json:"label"`
	Probability float32 `json:"probability"`
}

type ByProbability []LabelResult

func (a ByProbability) Len() int           { return len(a) }
func (a ByProbability) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByProbability) Less(i, j int) bool { return a[i].Probability > a[j].Probability }

func findBestLabels(probabilities []float32) []LabelResult {
	// Make a list of label/probability pairs
	var resultLabels []LabelResult
	for i, p := range probabilities {
		if i >= len(labels) {
			break
		}
		resultLabels = append(resultLabels, LabelResult{Label: labels[i], Probability: p})
	}
	// Sort by probability
	sort.Sort(ByProbability(resultLabels))
	// Return top 5 labels
	return resultLabels[:5]
}*/