

.PHONY: run train

run:
	go run -ldflags="-IC:/usr/include -LC:/usr/lib" main.go

train:
	python train.py